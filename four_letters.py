def load_data(valid_words) -> list[str]:
  with open(valid_words) as file:
    text = file.read()
    words = list(map(str, text.strip().split()))
  return words

print(load_data("letters.txt"))


def filter_list(file: list[str], initial: str, final: str) -> list[str]:
  return [each for each in file if len(each) == len(initial)]



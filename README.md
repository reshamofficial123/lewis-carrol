# Lewis Carrol


This is a group project where we are working on a game invented by Lewis Carrrol who wrote "Alice in Wonderland". In this game we can change one word into another by changing one letter at a time.
Explain that letters cannot be moved, merely substituted. Every time a letter is changed, it must result in an English word.
Given the following example 'wet to dry': wet - met - mat - may - day - dry.


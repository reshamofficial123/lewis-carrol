def load_data(valid_words) -> list[str]:
  with open(valid_words) as file:
    text = file.read()
    words = list(map(str, text.strip().split()))
  return words

def backtrack(word: str, target: str, soln = [], visited = []) -> list[str]:
    if word == target:
        soln += [target]
        return " ".join(soln)
    for indexToOmit in range(len(word)):
        for wordToCheck in load_data("letters.txt"):
            if len(wordToCheck) == len(word) and word[:indexToOmit] + word[indexToOmit+1:] == wordToCheck[:indexToOmit] + wordToCheck[indexToOmit+1:]:
                if wordToCheck not in visited and wordToCheck not in soln:        
                    return backtrack(wordToCheck, target, soln + [word], visited + [wordToCheck])
                

print(backtrack("house", "about"))                  
